<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210911224338 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE white_list_surveys ADD author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE white_list_surveys ADD CONSTRAINT FK_9C0443A2F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_9C0443A2F675F31B ON white_list_surveys (author_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE white_list_surveys DROP FOREIGN KEY FK_9C0443A2F675F31B');
        $this->addSql('DROP INDEX IDX_9C0443A2F675F31B ON white_list_surveys');
        $this->addSql('ALTER TABLE white_list_surveys DROP author_id');
    }
}
