<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220712191843 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE white_list_surveys CHANGE title title VARCHAR(255) NOT NULL, CHANGE question question VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE feed_back DROP FOREIGN KEY FK_ED592A60A76ED395');
        $this->addSql('ALTER TABLE feed_back DROP FOREIGN KEY FK_ED592A60F675F31B');
        $this->addSql('ALTER TABLE white_list_response DROP FOREIGN KEY FK_4E225319A76ED395');
        $this->addSql('ALTER TABLE white_list_response DROP FOREIGN KEY FK_4E2253191E27F6BF');
        $this->addSql('ALTER TABLE white_list_surveys DROP FOREIGN KEY FK_9C0443A282F1BAF4');
        $this->addSql('ALTER TABLE white_list_surveys DROP FOREIGN KEY FK_9C0443A2F675F31B');
        $this->addSql('ALTER TABLE white_list_surveys CHANGE title title LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE question question LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
