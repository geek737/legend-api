<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     attributes={"pagination_enabled"=false},
 *     normalizationContext={"groups"={"user"}, "enable_max_depth"=true}
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(
 *     fields="userNameGame",
 *     message="Ce nom d'utilisateur existe déja."
 * )
 * @UniqueEntity(
 *     fields="email",
 *     message="Cet email existe déja."
 * )
 * @UniqueEntity(
 *     fields="steamId"
 * )
 * @UniqueEntity(
 *     fields="discordId"
 * )
 * @ApiFilter(SearchFilter::class, properties={
 *     "userNameGame": "exact",
 *     "steamId": "exact",
 *     "discordId": "exact",
 *     "email": "exact",
 *     "statsWhitelist": "exact",
 *     "tokenChangingPassword": "exact"
 * })
 *
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $id;

    /**
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     * @ORM\Column(type="string", length=255)
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\Regex(
     *     "/^(?=[a-zA-Z0-9._\-ø]{5,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/",
     *     message="Ce nom n'est pas valide"
     * )
     * @Assert\Length(
     *     min=5,
     *     max = 20,
     *     )
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $userNameGame;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\Choice(
     *     {"accepted","pending","rejected","voice interview","none"},
     *     message="Cette valeur doit avoir 'accepted','pending','rejected','voice interview','none'"
     * )
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $statsWhitelist;

    /*
     * @Assert\Regex("/^steam:[a-z 0-9]+$/")
     * */
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern="/^steam:[a-z 0-9]{15}$/",
     *     message="Format steam incorrect"
     * )
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $steamId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex("/^\d{15,23}$/")
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $discordId;

    /**
     * @ORM\OneToMany(targetEntity=WhiteListResponse::class, mappedBy="user")
     * @Groups("user")
     */
    private $whiteListResponses;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Choice({"high","low"})
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $priority;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type("\DateTimeInterface")
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="json")
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=FeedBack::class, mappedBy="user")
     * @Groups("user")
     */
    private $feedBacks;

    /**
     * @ORM\OneToMany(targetEntity=FeedBack::class, mappedBy="author")
     * @Groups("user")
     */
    private $feedBacksWrited;

    /**
     * @ORM\OneToMany(targetEntity=WhiteListSurveys::class, mappedBy="author")
     * @Groups("user")
     */
    private $whiteListSurveys;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $isPasswordChanging;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $tokenChangingPassword;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type("\DateTimeInterface")
     * @Groups({"response", "user", "feedback", "surveys"})
     */
    private $urlCreatedAt;

    public function __construct()
    {
        $this->whiteListResponses = new ArrayCollection();
        $this->feedBacks = new ArrayCollection();
        $this->feedBacksWrited = new ArrayCollection();
        $this->whiteListSurveys = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserNameGame()
    {
        return $this->userNameGame;
    }

    /**
     * @param mixed $userNameGame
     */
    public function setUserNameGame($userNameGame): void
    {
        $this->userNameGame = $userNameGame;
    }

    /**
     * @return mixed
     */
    public function getStatsWhitelist()
    {
        return $this->statsWhitelist;
    }

    /**
     * @param mixed $statsWhitelist
     */
    public function setStatsWhitelist($statsWhitelist): void
    {
        $this->statsWhitelist = $statsWhitelist;
    }

    /**
     * @return mixed
     */
    public function getSteamId()
    {
        return $this->steamId;
    }

    /**
     * @param mixed $steamId
     */
    public function setSteamId($steamId): void
    {
        $this->steamId = $steamId;
    }

    /**
     * @return mixed
     */
    public function getDiscordId()
    {
        return $this->discordId;
    }

    /**
     * @param mixed $discordId
     */
    public function setDiscordId($discordId): void
    {
        $this->discordId = $discordId;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $cratedAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->userNameGame;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->userNameGame;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|WhiteListResponse[]
     */
    public function getWhiteListResponses(): Collection
    {
        return $this->whiteListResponses;
    }

    public function addWhiteListResponse(WhiteListResponse $whiteListResponse): self
    {
        if (!$this->whiteListResponses->contains($whiteListResponse)) {
            $this->whiteListResponses[] = $whiteListResponse;
            $whiteListResponse->setUser($this);
        }

        return $this;
    }

    public function removeWhiteListResponse(WhiteListResponse $whiteListResponse): self
    {
        if ($this->whiteListResponses->removeElement($whiteListResponse)) {
            // set the owning side to null (unless already changed)
            if ($whiteListResponse->getUser() === $this) {
                $whiteListResponse->setUser(null);
            }
        }

        return $this;
    }

    public function getPriority(): ?string
    {
        return $this->priority;
    }

    public function setPriority(string $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return Collection|FeedBack[]
     */
    public function getFeedBacks(): Collection
    {
        return $this->feedBacks;
    }

    public function addFeedBack(FeedBack $feedBack): self
    {
        if (!$this->feedBacks->contains($feedBack)) {
            $this->feedBacks[] = $feedBack;
            $feedBack->setUser($this);
        }

        return $this;
    }

    public function removeFeedBack(FeedBack $feedBack): self
    {
        if ($this->feedBacks->removeElement($feedBack)) {
            // set the owning side to null (unless already changed)
            if ($feedBack->getUser() === $this) {
                $feedBack->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FeedBack[]
     */
    public function getFeedBacksWrited(): Collection
    {
        return $this->feedBacksWrited;
    }

    public function addFeedBacksWrited(FeedBack $feedBacksWrited): self
    {
        if (!$this->feedBacksWrited->contains($feedBacksWrited)) {
            $this->feedBacksWrited[] = $feedBacksWrited;
            $feedBacksWrited->setAuthor($this);
        }

        return $this;
    }

    public function removeFeedBacksWrited(FeedBack $feedBacksWrited): self
    {
        if ($this->feedBacksWrited->removeElement($feedBacksWrited)) {
            // set the owning side to null (unless already changed)
            if ($feedBacksWrited->getAuthor() === $this) {
                $feedBacksWrited->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|WhiteListSurveys[]
     */
    public function getWhiteListSurveys(): Collection
    {
        return $this->whiteListSurveys;
    }

    public function addWhiteListSurvey(WhiteListSurveys $whiteListSurvey): self
    {
        if (!$this->whiteListSurveys->contains($whiteListSurvey)) {
            $this->whiteListSurveys[] = $whiteListSurvey;
            $whiteListSurvey->setAuthor($this);
        }

        return $this;
    }

    public function removeWhiteListSurvey(WhiteListSurveys $whiteListSurvey): self
    {
        if ($this->whiteListSurveys->removeElement($whiteListSurvey)) {
            // set the owning side to null (unless already changed)
            if ($whiteListSurvey->getAuthor() === $this) {
                $whiteListSurvey->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPasswordChanging()
    {
        return $this->isPasswordChanging;
    }

    /**
     * @param mixed $isPasswordChanging
     */
    public function setIsPasswordChanging($isPasswordChanging): void
    {
        $this->isPasswordChanging = $isPasswordChanging;
    }

    /**
     * @return mixed
     */
    public function getTokenChangingPassword()
    {
        return $this->tokenChangingPassword;
    }

    /**
     * @param mixed $tokenChangingPassword
     */
    public function setTokenChangingPassword($tokenChangingPassword): void
    {
        $this->tokenChangingPassword = $tokenChangingPassword;
    }

    /**
     * @return mixed
     */
    public function getUrlCreatedAt()
    {
        return $this->urlCreatedAt;
    }

    /**
     * @param mixed $urlCreatedAt
     */
    public function setUrlCreatedAt($urlCreatedAt): void
    {
        $this->urlCreatedAt = $urlCreatedAt;
    }
}
