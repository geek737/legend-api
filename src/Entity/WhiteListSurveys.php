<?php

namespace App\Entity;

use App\Repository\WhiteListSurveysRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ApiResource(
 *     attributes={"pagination_enabled"=false},
 *     normalizationContext={"groups"={"surveys"}, "enable_max_depth"=true}
 * )
 * @ORM\Entity(repositoryClass=WhiteListSurveysRepository::class)
 * @ApiFilter(SearchFilter::class, properties={
 *     "language.id": "exact"
 * })
 */
class WhiteListSurveys
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"language", "surveys", "response", "user"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"language", "surveys", "response", "user"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"language", "surveys", "response", "user"})
     */
    private $question;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Groups({"language", "surveys", "response", "user"})
     */
    private $images = [];

    /**
     * @ORM\OneToMany(targetEntity=WhiteListResponse::class, mappedBy="question")
     * @Groups("surveys")
     */
    private $response;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class, inversedBy="whiteListSurveys")
     * @Groups("surveys")
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="whiteListSurveys")
     * @Groups("surveys")
     */
    private $author;

    public function __construct()
    {
        $this->response = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getImages(): ?array
    {
        return $this->images;
    }

    public function setImages(array $images): self
    {
        $this->images = $images;

        return $this;
    }

    /**
     * @return Collection|WhiteListResponse[]
     */
    public function getResponse(): Collection
    {
        return $this->response;
    }

    public function addResponse(WhiteListResponse $response): self
    {
        if (!$this->response->contains($response)) {
            $this->response[] = $response;
            $response->setQuestion($this);
        }

        return $this;
    }

    public function removeResponse(WhiteListResponse $response): self
    {
        if ($this->response->removeElement($response)) {
            // set the owning side to null (unless already changed)
            if ($response->getQuestion() === $this) {
                $response->setQuestion(null);
            }
        }

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }
}
