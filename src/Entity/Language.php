<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LanguageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     attributes={"pagination_enabled"=false},
 *     normalizationContext={"groups"={"language"}, "enable_max_depth"=true}
 * )
 *
 * @ORM\Entity(repositoryClass=LanguageRepository::class)
 * @ApiFilter(SearchFilter::class, properties={
 *     "code": "exact"
 * })
 */

class Language
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"language", "surveys"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"language", "surveys"})
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"language", "surveys"})
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity=WhiteListSurveys::class, mappedBy="language")
     * @Groups("language")
     */
    private $whiteListSurveys;

    public function __construct()
    {
        $this->whiteListSurveys = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|WhiteListSurveys[]
     */
    public function getWhiteListSurveys(): Collection
    {
        return $this->whiteListSurveys;
    }

    public function addWhiteListSurvey(WhiteListSurveys $whiteListSurvey): self
    {
        if (!$this->whiteListSurveys->contains($whiteListSurvey)) {
            $this->whiteListSurveys[] = $whiteListSurvey;
            $whiteListSurvey->setLanguage($this);
        }

        return $this;
    }

    public function removeWhiteListSurvey(WhiteListSurveys $whiteListSurvey): self
    {
        if ($this->whiteListSurveys->removeElement($whiteListSurvey)) {
            // set the owning side to null (unless already changed)
            if ($whiteListSurvey->getLanguage() === $this) {
                $whiteListSurvey->setLanguage(null);
            }
        }

        return $this;
    }
}
