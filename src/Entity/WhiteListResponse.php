<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\WhiteListResponseRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ApiResource(
 *     attributes={"pagination_enabled"=false},
 *     normalizationContext={"groups"={"response"}, "enable_max_depth"=true}
 * )
 * @ORM\Entity(repositoryClass=WhiteListResponseRepository::class)
 * @ApiFilter(SearchFilter::class, properties={
 *     "user": "exact"
 * })
 */
class WhiteListResponse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"response", "surveys", "user"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="whiteListResponses")
     * @Groups("response")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=WhiteListSurveys::class, inversedBy="response")
     * @Groups("response")
     */
    private $question;

    /**
     * @ORM\Column(type="text")
     * @Groups({"response", "surveys", "user"})
     */
    private $response;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\Type("\DateTimeInterface")
     * @Groups({"response", "surveys", "user"})
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getQuestion(): ?WhiteListSurveys
    {
        return $this->question;
    }

    public function setQuestion(?WhiteListSurveys $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function setResponse(string $response): self
    {
        $this->response = $response;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}
