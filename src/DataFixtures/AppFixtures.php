<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\WhiteListSurveys;
use App\Repository\WhiteListSurveysRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderOnterface
     */
    private $encoder;
    /**
     * @var WhiteListSurveysRepository
     */
    private $listSurveysRepository;

    public function __construct(
        UserPasswordHasherInterface $encoder,
        WhiteListSurveysRepository $listSurveysRepository
    ) {
        $this->encoder = $encoder;
        $this->listSurveysRepository = $listSurveysRepository;
    }

    public function load(ObjectManager $manager)
    {
        $faker =  \Faker\Factory::create('fr_FR');
        $language = $this->listSurveysRepository->find(3);
        for ($i = 0; $i < 5; $i++) {
            $question = new WhiteListSurveys();
            $question->setTitle($faker->title);
            $question->setLanguage($language);
            $question->setQuestion($faker->paragraph());
            $manager->persist($question);
        }
//        for ($i = 0; $i < 5; $i++) {
//            $user = new User();
//            $user->setEmail("admin@admin.com");
//            $user->setRoles(["admin"]);
//            $plainPassword = 'test';
//            $encoded = $this->encoder->hashPassword($user, $plainPassword);
//
//            $user->setPassword($encoded);
//            $manager->persist($user);
//        }

        $manager->flush();
    }
}
