<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user/steamid/{id}", name="by_steam_id")
     */
    public function index(int $id)
    {
        return json_encode(['id' => $id]);
    }
}
