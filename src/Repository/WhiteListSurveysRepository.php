<?php

namespace App\Repository;

use App\Entity\WhiteListSurveys;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WhiteListSurveys|null find($id, $lockMode = null, $lockVersion = null)
 * @method WhiteListSurveys|null findOneBy(array $criteria, array $orderBy = null)
 * @method WhiteListSurveys[]    findAll()
 * @method WhiteListSurveys[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WhiteListSurveysRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WhiteListSurveys::class);
    }

    // /**
    //  * @return WhiteListSurveys[] Returns an array of WhiteListSurveys objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WhiteListSurveys
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
