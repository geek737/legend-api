<?php

namespace App\Repository;

use App\Entity\WhiteListResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WhiteListResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method WhiteListResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method WhiteListResponse[]    findAll()
 * @method WhiteListResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WhiteListResponseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WhiteListResponse::class);
    }

    // /**
    //  * @return WhiteListResponse[] Returns an array of WhiteListResponse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WhiteListResponse
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
